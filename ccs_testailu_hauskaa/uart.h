/*
 * uart.h
 *
 *  Created on: 27.3.2016
 *      Author: Joonas
 */


#include <stdint.h>


int uart_init(void);

int uart_getchar(void);

int uart_putchar(int c);

int uart_puts(const char *str);



